package com.rmpg.standaloneJPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CleanupTest
{
    private static EntityManagerFactory emf;

    private EntityManager em;

    @BeforeClass
    public static void initializeFactory()
    {
        emf = Persistence.createEntityManagerFactory("User");
    }

    @Before
    public void setupEM()
    {
        em = emf.createEntityManager();
    }

    @After
    public void cleanupEM()
    {
        em.close();
    }

    @Test
    public void removeExtraProgrammers()
    {
        Query loadQuery = em.createQuery("from Programmer", Programmer.class);
        List<Programmer> list = loadQuery.getResultList();

        em.getTransaction().begin();
        for (Programmer p : list)
        {
            if (p.getUsername().startsWith(JPATest.NAME_PREFIX))
                em.remove(p);
        }
        em.getTransaction().commit();
    }
}
