package com.rmpg.standaloneJPA;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

public class JPATest
{
    public static final String NAME_PREFIX = "Jane-";
    private static final String password = "p1";

    private static EntityManagerFactory emf;

    private EntityManager em;

    @BeforeClass
    public static void initializeFactory()
    {
        emf = Persistence.createEntityManagerFactory("User");
    }

    @Before
    public void setupEM()
    {
        em = emf.createEntityManager();
    }

    @After
    public void cleanupEM()
    {
        em.close();
    }

    private String persistTestUser(String nameSuffix)
    {
        final String username = NAME_PREFIX + nameSuffix + "-" + LocalDateTime.now().toString();

        Programmer p = new Programmer();
        p.setUsername(username);
        p.setPassword(password);
        p.setEnabled(true);

        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();

        return username;
    }

    private void assertEnabledInDb(String username, boolean expected)
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            Programmer programmer = em.find(Programmer.class, username);
            assertEquals(expected, programmer.isEnabled());
        }
        finally
        {
            em.close();
        }
    }
}
